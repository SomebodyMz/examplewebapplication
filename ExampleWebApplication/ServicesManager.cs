﻿using System;

using ExampleWebApplication.Db;
using ExampleWebApplication.Models;
using ExampleWebApplication.Services;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExampleWebApplication
{
    public class ServicesManager: Singleton<ServicesManager>
    {
        public IConfiguration Configuration { get; set; }

        public void AddStartupServices(IServiceCollection services) {
            if (Configuration == null) {
                throw new Exception("Startup configuration not found.");
            }

            services.AddDbContext<ApplicationContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddDbContext<UsersContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("IdentityConnection"));
            });

            services.AddControllersWithViews();

            services
                .AddIdentity<User, IdentityRole>(options => {
                    options.Password.RequiredLength = 6;
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<UsersContext>();

            services.AddMessageService<EmailMessageSender>();
        }
    }
}
