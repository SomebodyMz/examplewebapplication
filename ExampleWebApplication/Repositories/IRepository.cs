﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExampleWebApplication.Models;

namespace ExampleWebApplication.Repositories
{
    interface IRepository<T> where T : class
    {
        T GetItem(string id);

        IEnumerable<T> GetItems();

        IQueryable<T> GetUserItems(User user);

        void Create(T item);

        void Update(T item);

        void Delete(string id);

        void Save();
    }
}
