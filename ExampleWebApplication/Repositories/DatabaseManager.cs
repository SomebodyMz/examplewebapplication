﻿using System;

using ExampleWebApplication.Db;

namespace ExampleWebApplication.Repositories
{
    public class DatabaseManager: IDisposable
    {
        private ApplicationContext _db;

        private RequestsRepository _requestRepository;

        private bool disposed = false;

        public DatabaseManager(ApplicationContext context) {
            _db = context;
        }

        public RequestsRepository Requests {
            get {
                if (_requestRepository == null)
                    _requestRepository = new RequestsRepository(_db);
                return _requestRepository;
            }
        }

        public void Save() {
            _db.SaveChanges();
        }

        public virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    _db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
