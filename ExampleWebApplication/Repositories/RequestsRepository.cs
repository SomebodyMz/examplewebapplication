﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExampleWebApplication.Db;
using ExampleWebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace ExampleWebApplication.Repositories
{
    public class RequestsRepository : IRepository<Request>
    {
        private readonly ApplicationContext _db;

        public RequestsRepository(ApplicationContext context) {
            _db = context;
        }

        public IEnumerable<Request> GetItems() {
            return _db.Requests;
        }

        public IQueryable<Request> GetUserItems(User user) {
            var items = _db.Requests;

            return items.Where(i => i.CreatedBy == user.Id);
        }

        public Request GetItem(string id) {
            var dbKey = new Guid(id);
            return _db.Requests.Find(dbKey);
        }

        public void Create(Request request) {
            _db.Requests.Add(request);
        }

        public void Update(Request request) {
            _db.Entry(request).State = EntityState.Modified;
        }

        public void Delete(string id) {
            var dbKey = new Guid(id);
            var request = _db.Requests.Find(dbKey);

            if (request != null) {
                _db.Requests.Remove(request);
            }
        }

        public void Save() {
            _db.SaveChanges();
        }
    }
}
