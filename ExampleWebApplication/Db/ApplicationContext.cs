﻿using ExampleWebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace ExampleWebApplication.Db
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Request> Requests { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder) {
            builder.Entity<Request>().ToTable("cRequests");
            builder.Entity<Request>().Property(p => p.Content).IsUnicode(true);
            builder.Entity<Request>().Property(p => p.Title).IsUnicode(true);
            builder.Entity<Request>().Property(p => p.Title).HasMaxLength(1000);
        }
    }
}
