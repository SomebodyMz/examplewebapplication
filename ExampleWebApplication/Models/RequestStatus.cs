﻿namespace ExampleWebApplication.Models
{
    public enum RequestStatus
    {
        New,
        Approved,
        Declined,
    }
}
