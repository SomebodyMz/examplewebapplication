﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace ExampleWebApplication.Models
{
    public class User : IdentityUser
    {
        [MinLength(3)]
        public string DisplayName { get; set; }
    }
}
