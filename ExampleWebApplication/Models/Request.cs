﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExampleWebApplication.Models
{
    public class Request
    {
        [Key]
        public Guid Id { get; set; }

        public string CreatedBy { get; set; }

        public RequestStatus Status { get; set; }

        [Required]
        public string Title { get; set; }

        public string Content { get; set; }
    }
}
