﻿using System.ComponentModel.DataAnnotations;
using ExampleWebApplication.Models;

namespace ExampleWebApplication.ViewModels
{
    public class CreateRequestViewModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        public User User { get; set; }
    }
}
