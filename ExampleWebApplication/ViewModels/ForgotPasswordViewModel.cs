﻿using System.ComponentModel.DataAnnotations;

namespace ExampleWebApplication.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
