﻿using System.Collections.Generic;
using ExampleWebApplication.Models;
using Microsoft.AspNetCore.Identity;

namespace ExampleWebApplication.ViewModels
{
    public class ProfieViewModel
    {
        public User User { get; set; }

        public List<string> UserRoles { get; set; } = new List<string>();
    }
}
