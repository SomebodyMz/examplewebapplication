﻿using System.Collections.Generic;

using ExampleWebApplication.Models;

namespace ExampleWebApplication.ViewModels
{
    public class UsersRolesListViewModel
    {
        public List<User> UsersList { get; set; }
    }
}
