﻿using System.Collections.Generic;
using ExampleWebApplication.Models;

namespace ExampleWebApplication.ViewModels
{
    public class ChangeRequestViewModel
    {
        public bool CanEdit { get; set; } = false;

        public List<Request> Requests { get; set; } = new List<Request>();
    }
}
