﻿using System;

namespace ExampleWebApplication
{
    public abstract class Singleton<T> where T: Singleton<T>
    {
        private static readonly Lazy<T> _instance = new Lazy<T>(() => Activator.CreateInstance(typeof(T), true) as T);

        public static T Instance => _instance.Value;
    }
}
