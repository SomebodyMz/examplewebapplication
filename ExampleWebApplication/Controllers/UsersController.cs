﻿using ExampleWebApplication.Models;
using ExampleWebApplication.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleWebApplication.Controllers
{
    [Authorize(Roles = "admin")]
    public class UsersController : Controller
    {
        public UserManager<User> _userManager;

        /// <summary>
        /// UsersController constructor.
        /// </summary>
        public UsersController(UserManager<User> userManager) {
            _userManager = userManager;
        }

        /// <summary>
        /// Users list page.
        /// </summary>
        [HttpGet]
        public IActionResult Index() {
            return View(_userManager.Users.ToList());
        }

        /// <summary>
        /// Create user page.
        /// </summary>
        [HttpGet]
        public IActionResult Create() {
            return View();
        }

        /// <summary>
        /// Create user page (POST handler).
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel model) {
            if (ModelState.IsValid) {
                var user = new User {
                    Email = model.Email,
                    UserName = model.Email,
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded) {
                    await _userManager.AddToRoleAsync(
                        await _userManager.FindByEmailAsync(model.Email),
                        "user"
                    );

                    return RedirectToAction("Index");
                } else {
                    foreach (var error in result.Errors) {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return View(model);
        }

        /// <summary>
        /// Edit user page.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Edit(string id) {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null) {
                return NotFound();
            }

            var model = new EditUserViewModel {
                Id = user.Id,
                Email = user.Email,
            };

            return View(model);
        }

        /// <summary>
        /// Edit user page (POST handler).
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model) {
            if (ModelState.IsValid) {
                var user = await _userManager.FindByIdAsync(model.Id);
                if (user != null) {
                    user.Email = model.Email;
                    user.UserName = model.Email;

                    var result = await _userManager.UpdateAsync(user);

                    if (result.Succeeded) {
                        return RedirectToAction("Index");
                    } else {
                        foreach (var error in result.Errors) {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }

            return View(model);
        }

        /// <summary>
        /// Delete user page.
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> Delete(string id) {
            var user = await _userManager.FindByIdAsync(id);

            if (user != null) {
                await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }
    }
}
