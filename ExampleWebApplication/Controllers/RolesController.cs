﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExampleWebApplication.Models;
using ExampleWebApplication.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ExampleWebApplication.Controllers
{
    public class RolesController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly UserManager<User> _userManager;

        /// <summary>
        /// RolesController constructor.
        /// </summary>
        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<User> userManager) {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        /// <summary>
        /// Roles list page.
        /// </summary>
        [HttpGet]
        public IActionResult Index() {
            return View(_roleManager.Roles.ToList());
        }

        /// <summary>
        /// Create role page.
        /// </summary>
        [HttpGet]
        public IActionResult Create() {
            return View();
        }

        /// <summary>
        /// Create role page (POST handler).
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create(string name) {
            if (!string.IsNullOrEmpty(name)) {
                var result = await _roleManager.CreateAsync(new IdentityRole(name));

                if (result.Succeeded) {
                    return RedirectToAction("Index");
                } else {
                    foreach (var error in result.Errors) {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return View(name);
        }

        /// <summary>
        /// User roles page.
        /// </summary>
        [HttpGet]
        public IActionResult UserList() {
            return View(new UsersRolesListViewModel {
                UsersList = _userManager.Users.ToList(),
            });
        }

        /// <summary>
        /// Edit user roles page.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Edit(string userId) {
            var user = await _userManager.FindByIdAsync(userId);

            if (user != null) {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();

                var model = new ChangeRoleViewModel {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles,
                };

                return View(model);
            }

            return NotFound();
        }

        /// <summary>
        /// Edit user roles page (POST handler).
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles) {
            var user = await _userManager.FindByIdAsync(userId);

            if (user != null) {
                var userRoles = await _userManager.GetRolesAsync(user);
                var addedRoles = roles.Except(userRoles);
                var removedRoles = userRoles.Except(roles);

                await _userManager.AddToRolesAsync(user, addedRoles);
                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                return RedirectToAction("UserList");
            }

            return NotFound();
        }

        /// <summary>
        /// Delete role page.
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Delete(string id) {
            var role = await _roleManager.FindByIdAsync(id);

            if (role != null) {
                await _roleManager.DeleteAsync(role);
            }

            return RedirectToAction("Index");
        }

    }
}
