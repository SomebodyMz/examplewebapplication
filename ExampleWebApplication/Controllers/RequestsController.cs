﻿using System;
using System.Linq;

using ExampleWebApplication.Db;
using ExampleWebApplication.Models;
using ExampleWebApplication.Repositories;
using ExampleWebApplication.Services;
using ExampleWebApplication.ViewModels;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExampleWebApplication.Controllers
{
    [Authorize]
    public class RequestsController : Controller
    {
        private DatabaseManager _db;

        private readonly UserManager<User> _userManager;

        private readonly IMessageSender _messageSender;

        private readonly ILogger _logger;

        public RequestsController(ApplicationContext context, UserManager<User> userManager, IMessageSender messageSender, ILogger<RequestsController> logger) {
            _db = new DatabaseManager(context);
            _userManager = userManager;
            _messageSender = messageSender;
            _logger = logger;
        }

        /// <summary>
        /// Create request page.
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "user")]
        public IActionResult Add() {
            return View();
        }

        /// <summary>
        /// Create request page (POST handler).
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "user")]
        public IActionResult Add(CreateRequestViewModel model) {
            try {
                var newId = new Guid();

                _db.Requests.Create(new Request() {
                    Id = newId,
                    Title = model.Title,
                    Content = model.Description,
                    CreatedBy = _userManager.GetUserId(User),
                    Status = RequestStatus.New,
                });

                _db.Save();

                ViewBag.Result = "Saved.";

                _logger.LogInformation("Saved new request {ID}", newId);
            } catch (Exception e) {
                ViewBag.Error = "Save error:\n";
                ViewBag.Error += e.InnerException != null ?
                    e.InnerException.Message :
                    e.Message;

                _logger.LogError(e, "Error saving new request");
            }

            return View();
        }

        /// <summary>
        /// Manage requests.
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult List() {
            return View(new ChangeRequestViewModel {
                Requests = _db.Requests.GetItems().ToList(),
                CanEdit = true,
            });
        }

        /// <summary>
        /// View own requests list.
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "user")]
        public IActionResult ListByUser() {
            var currUser = _userManager.GetUserAsync(User).Result;

            return View("List", new ChangeRequestViewModel {
                Requests = _db.Requests.GetUserItems(currUser).ToList(),
                CanEdit = false,
            });
        }

        /// <summary>
        /// Approve request.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Approve(string id) {
            var currRequest = _db.Requests.GetItem(id);

            currRequest.Status = RequestStatus.Approved;
            _db.Save();

            var user = _userManager.GetUserAsync(User).Result;
            var message = $"Your request '{currRequest.Title}' has been APPROVED.\n Congratulations.";
            _messageSender.Send("admin@admin", user.Email, message);

            return View("List", new ChangeRequestViewModel {
                Requests = _db.Requests.GetItems().ToList(),
                CanEdit = true,
            });
        }

        /// <summary>
        /// Approve request.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Decline(string id) {
            var currRequest = _db.Requests.GetItem(id);

            currRequest.Status = RequestStatus.Declined;
            _db.Save();

            var user = _userManager.GetUserAsync(User).Result;
            var message = $"Your request '{currRequest.Title}' has been DECLINED.\n Sorry about that.";
            _messageSender.Send("admin@admin", user.Email, message);

            return View("List", new ChangeRequestViewModel {
                Requests = _db.Requests.GetItems().ToList(),
                CanEdit = true,
            });
        }
    }
}
