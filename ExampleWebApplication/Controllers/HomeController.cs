﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ExampleWebApplication.Models;
using System.Reflection;
using System.Runtime.Versioning;

namespace ExampleWebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger) {
            _logger = logger;
        }

        /// <summary>
        /// Index (main) page.
        /// </summary>
        [HttpGet]
        public IActionResult Index() {
            ViewBag.Framework = Assembly.GetEntryAssembly()?.GetCustomAttribute<TargetFrameworkAttribute>()?.FrameworkName;
            ViewBag.Platform = System.Runtime.InteropServices.RuntimeInformation.OSDescription;

            _logger.LogInformation("Index page called.");

            return View();
        }

        /// <summary>
        /// About page.
        /// </summary>
        [HttpGet]
        public IActionResult About() {
            ViewData["Title"] = "About Page";
            ViewData["Message"] = "This is application description page.";

            return View();
        }

        /// <summary>
        /// Error page.
        /// </summary>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
