﻿using System.Web.Http;
using ExampleWebApplication.Db;
using ExampleWebApplication.Models;
using Microsoft.AspNetCore.Identity;

namespace ExampleWebApplication.ApiControllers
{
    [Authorize(Roles = "admin")]
    public class ManageRequestsController : ApiController
    {
        private readonly ApplicationContext _context;

        private UserManager<User> _userManager;

        public ManageRequestsController(ApplicationContext context, UserManager<User> userManager) {
            _context = context;
            _userManager = userManager;
        }

        /// <summary>
        /// Approve request.
        /// </summary>
        [HttpPost]
        public bool Approve(string id) {

            return true;
        }

        /// <summary>
        /// Approve request.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public bool Decline(string id) {

            return true;
        }
    }
}
