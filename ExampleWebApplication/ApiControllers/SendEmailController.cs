﻿using System.Web.Http;
using ExampleWebApplication.Services;

namespace ExampleWebApplication.ApiControllers
{
    public class SendEmailController : ApiController
    {
        private readonly IMessageSender _messageSender;

        public SendEmailController(IMessageSender messageSender) {
            _messageSender = messageSender;
        }

        [HttpPost]
        public void SendMail([FromBody]string from, [FromBody]string to, [FromBody]string message, [FromBody]string subject = null) {
            _messageSender.Send(from, to, message, subject);
        }
    }
}
