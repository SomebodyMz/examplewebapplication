﻿using System.IO;
using System.Net.Mail;

namespace ExampleWebApplication.Services
{
    public class EmailMessageSender : IMessageSender
    {
        public static string SAVE_DIR = @"C:\temp";

        public void Send(string from, string to, string message, string subject = null) {
            using var client = new SmtpClient("savetofile") {
                DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory,
                PickupDirectoryLocation = SAVE_DIR,
            };

            if (!Directory.Exists(SAVE_DIR)) {
                Directory.CreateDirectory(SAVE_DIR);
            }

            client.SendMailAsync(new MailMessage(from, to, subject, message));
        }
    }
}
