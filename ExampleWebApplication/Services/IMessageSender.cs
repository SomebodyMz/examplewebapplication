﻿namespace ExampleWebApplication.Services
{
    public interface IMessageSender
    {
        void Send(string from, string to, string message, string subject = null);
    }
}
