﻿using Microsoft.Extensions.DependencyInjection;

namespace ExampleWebApplication.Services
{
    public static class ServiceExtensions
    {
        public static void AddMessageService<T>(this IServiceCollection services) where T: class, IMessageSender {
            services.AddTransient<IMessageSender, T>();
        }
    }
}
