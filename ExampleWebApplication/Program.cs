using System;
using System.Threading.Tasks;
using ExampleWebApplication.Db;
using ExampleWebApplication.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace ExampleWebApplication
{
    public class Program
    {
        public static async Task Main(string[] args) {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            var host = CreateHostBuilder(args).Build();

            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            try {
                await ApplicationInit.InitializeAsync(
                    services.GetRequiredService<UserManager<User>>(),
                    services.GetRequiredService<RoleManager<IdentityRole>>()
                );
            } catch (Exception ex) {
                logger.Error(ex, "An error occurred while seeding the database.");
            } finally {
                NLog.LogManager.Shutdown();
            }
            
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging => {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(LogLevel.Debug);
                })
                .UseNLog();
    }
}
